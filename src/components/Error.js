import React from "react";

function Error({ error }) {
  return <div>{error.msg}</div>;
}

export default Error;
